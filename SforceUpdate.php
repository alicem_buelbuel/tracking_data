<?php
ini_set('auto_detect_line_endings',TRUE);
error_reporting(E_ALL);
require('vendor/autoload.php');
require_once('csv_to_array.php');

$file = $argv[1];



try {
	/*
	 * CSV Mapping
	 */
	$csv = csv_to_array($file);
} catch (Exception $e) {
	echo 'csv mapping exception: ', $e->getMessage(), "\n";
}

try {
	/*
	 * SOAP login
	 */
	$credentials = json_decode(file_get_contents('assets/credentials.json'));

	$builder = new \Phpforce\SoapClient\ClientBuilder(
		$credentials->soapconnection->sandbox->wsdl,
		$credentials->soapconnection->sandbox->email,
		$credentials->soapconnection->sandbox->password,
		$credentials->soapconnection->sandbox->token
	);

	$client = $builder->build();
} catch (Exception $e) {
	echo 'soap login exception: ', $e->getMessage(), "\n";
}

try {
	/*
	 * SOAP Update
	 */
	$sObject = new stdClass();
	foreach ($csv as $key) {
		$sObject->Id = $key['Id'];
		$sObject->Tracking_ID__c = $key['tracking_id'];
		$results[] = $client->update(array($sObject), 'Purchase__c');
		//print_r($sObject); // debug
	}
	//var_dump($results); // debug
} catch (Exception $e) {
	echo 'soap update exception: ', $e->getMessage(), "\n";
}

