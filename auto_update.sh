#!/bin/bash
DIR_TODO="./to_process"
DIR_PROCESSED="./processed"

FILES_TO_PROCESS=( `diff -x *.tmp -x *.csv.* -x .DS_Store -r $DIR_TODO $DIR_PROCESSED | awk '{print $4}'` )
for FILE in ${FILES_TO_PROCESS[@]}; do
	
	#php ./SforceUpdate.php $FILE
	echo $FILE
	if [ $? -ne 0 ]; then
		echo "error processing $FILE"
	else
		ln -s $DIR_TODO/$FILE $DIR_PROCESSED/$FILE
	fi
done >> log.txt

