import email
import getpass, imaplib
import os
import sys
import json

detach_dir = '.'
data_dir = 'to_process'

if data_dir not in os.listdir(detach_dir):
    os.mkdir(data_dir + '/')

credentials = json.load(open('assets/credentials.json'))

userName = credentials["gmailconnection"]["username"]
passwd = credentials["gmailconnection"]["password"]
senderName = 'dmg-ag'

try:
    imapSession = imaplib.IMAP4_SSL('imap.gmail.com')
    typ, accountDetails = imapSession.login(userName, passwd)
    if typ != 'OK':
        print 'Error: login'
        raise Exception('Error: login')

    imapSession.select('[Gmail]/All Mail')
    typ, data = imapSession.search(None, 'FROM', senderName)
    if typ != 'OK':
        print 'Error: searching inbox'
        raise Exception('Error: searching inbox')

    for msgId in data[0].split():
        typ, messageParts = imapSession.fetch(msgId, '(RFC822)')
        if typ != 'OK':
            print 'Error: fetching mail'
            raise Exception('Error: fetching mail')

        emailBody = messageParts[0][1]
        mail = email.message_from_string(emailBody)
        for part in mail.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            fileName = part.get_filename()

            if bool(fileName):
		fileName = fileName.replace(' ', '_')
                filePath = os.path.join(detach_dir, data_dir + '/', fileName)
                if not os.path.isfile(filePath):
                    print fileName
                    fp = open(filePath, 'wb')
                    fp.write(part.get_payload(decode=True))
                    fp.close()

    imapSession.close()
    imapSession.logout()

except Exception as e:
    print(e)
